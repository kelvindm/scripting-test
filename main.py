import os
import re

import requests
from datetime import datetime

if not os.path.exists('Data'):
    os.mkdir('Data')

get_from_internet = requests.get('https://en.wikipedia.org/wiki/Python_(programming_language)')
result = get_from_internet.text
fetch_time = datetime.now().strftime("%Y-%m-%d%H:%M:%S")
os.chdir('Data')
file_name = f'WIKIPEDIA_PYTHON_{re.sub("[^a-zA-Z0-9]+", "_", fetch_time)}.txt'
with open(file_name, 'w+', encoding='utf-8') as f:
    f.write(result)
    f.close()
